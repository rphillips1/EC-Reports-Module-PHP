<?php

/**
 * @file
 * New iteraction of the sales view using a new categorization system in an 
 * attempt to better display results for ecreports. The main purpose of this file 
 * is to create a default well formed distributor array and determine this distributors
 * sales figures from a database table of sales info.
 * The salesview view is a pluggable view using the MVC paradigm for the 
 * ecreports module.
 * 
 * @see ecreports.sales.inc
 *
 */

/**
 * Main page_callback function to create the html for the reports page 
 *
 * @return array
 *  Returns the $page['#markup'] array for Drupal rendering 
 */
function  ecreports_view_salesview() {
  global $user;
  // @TODO Explain
  if(isset($_GET['did']) && is_numeric($_GET['did'])) {
    $did = check_plain((int)$_GET['did']);
    // Determine the years to display based on the current date
    $month = date('n');
    $end = $month > 1 ? date('Y') : date('Y') - 1;
    $start = $month > 1 ? date('Y') - 1 : $end - 1;
	#$start = 2;
	#$end = 14;
    // Get a well formed distributor array
    $distributor = ecreports_view_salesview_get_distributor($did, $start, $end);

    if($distributor) {
      $distributor = ecreports_view_salesview_get_sales($distributor, $start, $end);
      $markup = ecreports_view_salesview_get_report_page($distributor);
    }
    else {
      $markup = 'This is not a correct distributor number.';
    }
  }
   elseif(!isset($user->roles[12])) {
      $form = drupal_get_form('ecreports_view_salesview_add_select_form');
      $markup = drupal_render($form); 
    }
  return $page = array('#markup' => $markup);
}

##################### Section for get Distributor ###############

/**
 * Function to create the well formed distributor array
 * 
 * @param $did int
 *  The Emuge distributor customer number
 *
 * @return array|false
 *  Return a well formed distributor array or false if the provided $did 
 *  yielded no results from the database.
 */
function ecreports_view_salesview_get_distributor($did, $start = 2014, $end = 2015) {
  $distTable = 'emugecom_ecdistributor_properties';
  $query = db_select($distTable, 'dis')->fields('dis', array('did', 'name'));
  $query->condition('did', $did);
  
  try {
    $results = $query->execute()->fetchAssoc(); 
  } catch(PDOException $e) {
    $results = false;
  }

  if($results !== false) {
    $distributor = array(
      'did' => $results['did'],
      'name' => $results['name'],
      'data' => array(
        'header' => array('', 'YTD', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
        'tables' => array(),
        ),
    );
    
    $cats = ecreports_view_salesview_get_category_classes();
    pc($cats, '$cats');
    foreach($cats as $weight => $c) {
       $distributor['data']['tables'][$weight] = ecreports_view_salesview_add_table($c, $start, $end);
    }
  } 
  else {
    $distributor = false;
  }
  
  return $distributor;
}

/**
 * Function to retrieve and organize our current Class Categories
 * per our table configuration. 
 *
 * @TODO move to model file, cache these results
 *
 * @return array
 */
function  ecreports_view_salesview_get_category_classes() {
  $catMapTable   = 'emugecom_class_category_map';
  $categoryTable = 'emugecom_item_class_categories';
  $ccd = array(); // Variable for class category data
  
  // Gather the relationships in our map table and join ids with labels
  $query = db_select($catMapTable, 'ccm')->fields('ccm', array('class_category', 'item_class'));
  $query->join($categoryTable, 'itc', 'itc.id = ccm.class_category');
  $query->fields('itc', array('category_label', 'weight'))->orderBy('weight');
  // Attempt the search and pc log the results
  try {
    $classes = $query->execute()->fetchAll();
    
  } catch(PDOException $e) {
    watchdog('ecreports', $e->getMessage(), WATCHDOG_ERROR);
  }
  
  // Setup this category's initial framework for the finished array
  foreach($classes as $c) {
    // Utilize the weight column for ordering of the tables
    if(!isset($ccd[$c->weight])) {
      $ccd[$c->weight] = array(
        'id' => (int)$c->class_category,  // The arbitrary category id
        'label' => $c->category_label,    // The human readable name of the category
        'classes' => array(),             // The collection of classes to be used as column names 
      );
    }
    
    // Add this records CLASSXX to the 'classes' array of this category
    $ccd[$c->weight]['classes'][] = $c->item_class;  // Like CLASSXX 
  }

  return $ccd;
}

/**
 * Function to add an individual tables to our distributor
 *
 * @param $c array
 *  info to use in the row
 *
 * return array
 *  Return an empty set of rows suitable for theme table and meta data
 */
function ecreports_view_salesview_add_table($c, $label1 = 2014, $label2 = 2015) {
  $table = array(
    'title' => $c['label'] . ' Sales',
    'id' => (int)$c['id'],
    'classes' => $c['classes'],
    'rows' => array(
      $label1 => ecreports_view_salesview_add_row($label1),
      $label2 => ecreports_view_salesview_add_row($label2),
      'Difference' => ecreports_view_salesview_add_row('Difference'),
      'Change' => ecreports_view_salesview_add_row('Change'),
    ),
  );

  return $table;
}

/**
 * Function to add an individual row with proper header column data 
 *
 * @param $rid string
 *  Title for use in the first cell of the row
 *
 * return array
 *  Return a row suitable for theme table
 */
function ecreports_view_salesview_add_row($rid) {
  $row = array();
  $row = array_pad($row, 14, 0);
  
  if($rid == 'Difference') {
    $rowTitle = 'Difference($)';
  }
  elseif($rid == 'Change') {
    $rowTitle = 'Change(%)';
  }
  
  $row[0] = isset($rowTitle) ? $rowTitle : $rid;
  
  return $row;
}

############### Section for get sales functions ##############

/**
 * Function to retrieve the sales info using the well formed distributor array
 * 
 * @param $distributor array
 *  The well formed distributor array
 *
 * @param int $start
 *  Table row 1 starting year
 *
 * @param int $end
 *  Tabel row 2 ending year 
 *
 * @return array
 *  The updated distributor array
 */
function ecreports_view_salesview_get_sales($distributor, $start = 2014, $end = 2015) {
  $salesTable = 'emugecom_ecreports_sales';
  
  // Adding the ability for non scalar dids
  $did = _ecreports_get_did_grouping($distributor['did']);
  // Adding the ability for non scalar dids
  $operand = is_array($did) ? 'IN' : '=';
  $co = $distributor['name'];
  $title = "$end Sales Report - $co";
  drupal_set_title($title);
  foreach($distributor['data']['tables'] as $weight => $table) {
    $fields =  $table['classes'];
    $fields[] = 'did';
    $fields[] = 'mo_sales';
    $fields[] = 'month';
    $fields[] = 'year';
    
    $query = db_select($salesTable, 's')->fields('s', $fields);
    $query->condition('did', $did, $operand);
    $query->condition('year', array($start, $end), 'IN');
    
    try {
     $records = $query->execute()->fetchAll();
     
    } catch (PDOException $e) {
      watchdog('ecreports', $e->getMessage(), WATCHDOG_ERROR);
      $records = false;
    }
    // Add the values of any record's CLASSXX columns for that month
    if($records !== false) {
      foreach($records as $record => $data) {
        foreach($data as $key => $d) {
          if(preg_match('/CLASS/', $key) != 0) {
            $distributor['data']['tables'][$weight]['rows'][$data->year][$data->month + 1] += $d;
          }
        }
      }
    }
    // Iterate the row index and calculate differences, totals, and change
    foreach($distributor['data']['tables'][$weight]['rows'][$start] as $index => $cell) {
      $monthStop = date('n') + 1;
      if($index > 1 && $index < $monthStop) {
        $distributor['data']['tables'][$weight]['rows'][$start][1] += (int)$distributor['data']['tables'][$weight]['rows'][$start][$index];
        $distributor['data']['tables'][$weight]['rows'][$end][1] += (int)$distributor['data']['tables'][$weight]['rows'][$end][$index];
        
        $distributor['data']['tables'][$weight]['rows']['Difference'][$index] = 
          $distributor['data']['tables'][$weight]['rows'][$end][$index] - $distributor['data']['tables'][$weight]['rows'][$start][$index];
        
        $distributor['data']['tables'][$weight]['rows']['Difference'][1] += $distributor['data']['tables'][$weight]['rows']['Difference'][$index];
        
        if($distributor['data']['tables'][$weight]['rows'][$start][$index] != 0) {
          $distributor['data']['tables'][$weight]['rows']['Change'][$index] = 
            ($distributor['data']['tables'][$weight]['rows']['Difference'][$index] / $distributor['data']['tables'][$weight]['rows'][$start][$index]) * 100;
        }
        else {
          $distributor['data']['tables'][$weight]['rows']['Change'][$index] = '-';
        }
      }
      elseif($index >= $monthStop) {
        // With the remaining months, post $monthStop, we display a neater '-' instead of zero 
        $distributor['data']['tables'][$weight]['rows'][$end][$index] = '-';
        $distributor['data']['tables'][$weight]['rows']['Difference'][$index] = '-';
        $distributor['data']['tables'][$weight]['rows']['Change'][$index] = '-';
      }
      // At the end of this tables index, calculate this tables ytd change value
      if($index = 13) {
        // cannot divide by zero
        if($distributor['data']['tables'][$weight]['rows'][$start][1] != 0) {
          $distributor['data']['tables'][$weight]['rows']['Change'][1] = 
            ($distributor['data']['tables'][$weight]['rows']['Difference'][1] /
            $distributor['data']['tables'][$weight]['rows'][$start][1]) 
            * 100;
        }
        // Or set it '-' 
        else {
          $distributor['data']['tables'][$weight]['rows']['Change'][1] = '-';
        }
        
      }
    } // for each $index
    
    // Format this table for display    
    $distributor['data']['tables'][$weight]['rows'] = ecreports_view_salesview_format_table($distributor['data']['tables'][$weight]['rows']);
  } // for each $table
  
  return $distributor;
}

/**
 * Function to format an individual tables for display
 *
 * @param $table array
 *  The table to be formatted
 * 
 * @return array
 *  The formatted table
 */
function ecreports_view_salesview_format_table($table, $precision = 0) {
  // For all rows except Change, format the values as numbers that
  // are not '-'
  
  foreach($table as $row => $data) {
    if($row != 'Change') {
      foreach($data as $key => $val) {
        if($key > 0 && $val != '-') {
          $table[$row][$key] = number_format((float)$val, $precision);
        }
      }
    }
    else {
      // Precision logic to maintiain percentage readability
      foreach($data as $key => $val) {
        if($key > 0) {
          if(abs($val) >= 100) {
            $precision = 0;
          }
          elseif(abs($val) >= 10) {
            $precision = 1;
          }
          else {
            $precision = 2;
          }
          
          if($val != '-') {
            $table[$row][$key] = number_format($val, $precision);
          }
        }
      }
    }
  }
  return $table;
}

############### Section for building html for page #############

/**
 * Function to convert the distributor data tables into html
 *
 * @param $distributor array
 *  The pre-built distributor array to be processed 
 *
 * @return string
 *  Returns html markup ready to be added to the $page object
 */
function ecreports_view_salesview_get_report_page($distributor) {
  // Declare needed page variables @TODO explain
  $header = $distributor['data']['header'];
  $name = $distributor['name'];
  // Added logic to handle multiple dids as $did -rp
  $did = is_array($distributor['did']) ? $distributor['did'][0] : $distributor['did'];
  $path = check_plain($_GET['q']);
  // Make the back link contextual and correct. If the destination is set, add the custom path
  if(isset($_GET['destination']) && $_GET['destination'] == 'elite') {
    $backLink = "<a href=\"/distributor/elite-report-v11/{$did}\">Back</a>";
  }
  else {
    $backLink = "<a href=\"/ecreports/distributor?did={$did}\">Back</a>";
  }
  // Turn off the backlinks for now 1/25/16 -rp
  $backLink = '';
  // Links for providing pdf and print features
  $links = 
      '<div class="ec-tpr-links hide-links">
        <a href="/printpdf/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link"  onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">pdf</a>
        <a href="/print/' . $path . '?did=' . $did .'" style="float:right;"  class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">print</a>
      </div>';
  // Combine $backlink, $links, $name, and $did into a page heading and add to $markup
  $markup = "{$backLink}{$links}<h3 class=\"ecreports\">Sales Report for {$name} (Account #{$did})</h3>";
  // Add a rendered table to $markup for each set of data contained in the [data][tables] array
  foreach($distributor['data']['tables'] as $weight => $table) {
    $markup .= "<h6 class=\"ecreports\">{$table['title']}</h6>";
    $markup .= theme('table', array(
      'header' => $header, 
      'rows' => $table['rows'],
      'attributes' => array(
        'class' => array('ecreports', ),
      ),
    ));
  }
  
  return $markup;
}

/**
 * Function to create the salesview distributor select form
 *
 * @param $form object
 *  The pre-built drupal provided $form object
 *
 * @param $form_state object
 *  The pre-built drupal provided $form_state object
 *
 * @return object
 *  Returns a renderable form object
 */
function ecreports_view_salesview_add_select_form($form, &$form_state) {
  // Setup for group id use
  $gid = isset($_GET['gid']) ? check_plain($_GET['gid']) : null;
  $type = !is_null($gid) ? 'group' : null;
  $options = _ecreports_view_salesview_get_options($type, $gid);
  $form = array();
  $title = "Select your distributor {$type}";
  $title = ($type == 'group') ? 'Sales Area Reports:<br />Select an area to see it\'s related goals' : 'Select a distributor';
  // Check to see if the did is present in the select options and assign it to the default value
  if(isset($_GET['did']) && is_numeric($_GET['did']) && isset($options[$_GET['did']])) {
    $did = check_plain($_GET['did']);
  }
  if($gid) {
    $did = check_plain($_GET['did']);
    $form['gid'] = array(
      '#type' => 'markup',
      '#markup' => '<label>Sales Management Report:<br />Click <a href="/distributor/national-elite-report-v12/' . $did . '">here</a> for goals related to the corporate agreement.</label>',
    );
  }
  
  $form['did'] = array(
    '#type' => 'select',
    '#title' => $title,
    '#options' => $options,
    '#default_value' => isset($did) ? $did : '',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Select',
  );
  
  return $form;
}

/**
 * Function to attach additional $dids to a given $did per a grouping table. This build
 * will return an array of the entire relationship, regardless of its hierarchy. Children
 * parents return the same array.
 *
 * @param $did string
 *  A distributor number suitable for searching our grouping table with
 * 
 * @return string | array
 *  Returns the same given $did as a string if not results were found in the table or
 *  returns an array of $dids to use in the query if a grouping relationship was found.
 */
function _ecreports_get_did_grouping($did) {
  $groupTable = 'emugecom_ecreports_did_grouping_map';
  // Search both columns for a potential relationship
  $db_or = db_or();
  $db_or->condition('pdid', $did);
  $db_or->condition('cdid', $did);
  // Return both columns to build the list to search for
  $query = db_select($groupTable, 'gt')->fields('gt', array('pdid', 'cdid'));
  $query->condition($db_or);
  // Try catch our query
  try {
    $results = $query->execute()->fetchAll();
  } 
  catch(PDOException $e) {
    $results = 0;
  }
  // Only declare $dids if results are found for
  // return value logic
  if(count($results)) {
    $dids = array();
    foreach($results as $key => $val) {
      // Set the parent $did at the zero index
      if(!isset($dids[0])) {
        $dids[0] = $val->pdid; 
      }
      // Append child $did to list 
      $dids[] = $val->cdid;
    }
  }
  // $dids would exists if $results were found else give back
  // the provided $did
  $return = isset($dids) ? $dids : $did;
  
  return $return;
}

/*
 * Submit handler for the salesview form redirection
 *
 * @param $form object
 *  drupal form object
 *
 * @param $form_state
 *  drupal form_state object
 */
function ecreports_view_salesview_add_select_form_submit($form, &$form_state) {
  // Sanitize url input
  $did = check_plain($form_state['values']['did']);
  $q = check_plain($_GET['q']);
  $gid = isset($_GET['gid']) ?  check_plain($form_state['values']['did']) : null;
  // Set the page title and redirect
  if(!is_null($gid)) {
    drupal_set_title('Elite Distributor Regional Report Page');
    drupal_goto('/distributor/regional/elite-report-v11/' . $gid);
  }
  elseif(preg_match('/salesview/', $q) == true && $did != null) {
    drupal_set_title('Distributor #' . $did . ' Sales Totals by Item Class');
    drupal_goto('/ecreports/salesview', array('query' => array('did' => $did)));
  }
  // Provide title and redirect information for a particular distributor view
  elseif($did != null) {
    drupal_set_title('Distributor #' . $did . ' Information Page');
    drupal_goto('/ecreports/distributor', array('query' => array('did' => $did)));
  }
}

##############################################################################
## Fucntions that are to become the api for populating a distributors/staff ##
## of distributors select options wherever it occurs.                       ##
##############################################################################
/**
 * Provide appropriate options to the select field of our distributor select form
 *
 * @return array
 *  options list suitable for a form
 */
function  _ecreports_view_salesview_get_options($type = null, $id = null) {
  global $user;
  $uid = $user->uid;
  $user = user_load($uid);
  $options = array();
  $did = isset($_GET['did']) ? check_plain($_GET['did']) : null;
  
  switch($type) {
    case 'group':
      // Check if the user is a Territory Manager
      if(isset($user->roles[7]) || isset($user->roles[11]) || isset($user->roles[3]) || isset($user->roles[12]) || isset($user->roles[6])) {
        // Test UIDs with particular users
        #$uid = ($uid == '37') ? '337' : $uid; // Bob Adamiak
        $uid = ($uid == '37') ? '1111' : $uid; // Tom Jenkins
                
        $map = 'emugecom_gid_uid_map';
        
        $query = db_select($map, 'm')->fields('m');
        $query->join('eck_regional_elite_groups', 'eg', 'm.gid = eg.id');
        $query->fields('eg', array('title'))->distinct();
        
        if(isset($user->roles[7])) {
          #$query->condition('m.uid', $uid);
        }
        
        if(isset($user->roles[12]) || !is_null($did)) {
          if(is_null($did)) {
            $user = user_load($uid);
            $did = $user->field_distributor_id['und'][0];
          }
          $query->condition('m.did', $did);
        }
        
        $results = $query->execute()->fetchAll();
      }
      elseif(isset($user->roles[6])) {
      }
      else {
        $results = false;
      }
    break;
    default;
      $distTable = 'emugecom_ecdistributor_properties';
      $distReportTable = 'emugecom_ecreports_sales';
      $custTable = 'customer_data';
      // Check if the user is a Territory Manager  ////////////////////
      if(isset($user->roles[7])) {
        $tid_didMap = 'emugecom_tid_did_map';
        $user = user_load($uid, true);
        // Check if the user has territory numbers assigned and prepare them for query use
        if(isset($user->field_emuge_tmn['und']) && count($user->field_emuge_tmn['und'])) {
          $tmns = array();
          foreach($user->field_emuge_tmn['und'] as $tm) {
            $tmns[] = $tm['value'];
          }
        }
        // If user has tids, get thier allowed dids from the mapping table
        // @TODO join the table from the second query here and eliminte the need for two queries
        if(isset($tmns)) {
          $query = db_select($tid_didMap, 'tdm')->fields('tdm', array('did'));
          $query->condition('tid', $tmns, 'IN');
          
          try {
            $results = $query->execute()->fetchAll();
          } 
          catch(PDOException $e) {
            $results = false;
          }
          // Format results for query
          if(count($results)) {
            $dids = array();
            foreach($results as $r) {
              $dids[] = $r->did;
            }
          }
          // Provide default results of false
          else {
            $results = false;
          }
        }
        // Get the name and id info of allowed distributors
        if(isset($dids)) {
          $query = db_select($custTable, 'd');
          #$query = db_select($distReportTable, 'drt')->fields('drt', array('did'));
          #$query->join($custTable, 'd', 'd.CustomerNumber = drt.did');
          #$query->fields('d', array('name'))->condition('d.did', $dids, 'IN')->orderBy('name');
          $query->addField('d', 'CustomerNumber', 'did');
          $query->addField('d', 'CustomerName', 'name');
          $query->addField('d', 'DiscountCode', 'disc');
          $query->groupBy('did');
          $query->orderBy('name');
          $query->condition('d.CustomerNumber', $dids, 'IN');
          try {
            $results = $query->execute()->fetchAll();
          } 
          catch(PDOException $e) {
            $results = false;
          }
          
        }
        // Provide default results of false
        else {
          $results = false;
        }
      }
      // Check if user is Emuge Staff  ////////////////////////
      elseif(isset($user->roles[6])) {
        // Gather available distributor dids, name from the reports table, properties
        #$query = db_select($distReportTable, 'd')->fields('d', array('did'));
        #$query->join($distTable, 'dt', 'd.did = dt.did');
        #$query->fields('dt', array('name'))->orderBy('name');
        $query = db_select($custTable, 'd');
        $query->addField('d', 'CustomerNumber', 'did');
        $query->addField('d', 'CustomerName', 'name');
        $query->addField('d', 'DiscountCode', 'disc');
        $query->groupBy('did');
        $query->orderBy('name');
        
        try {
          $results = $query->execute()->fetchAll();
        } catch(PDOException $e) {
          $results = false;
        }
      }
      // Provide results for an authenticated users distributor number
      elseif(count($user->roles) == 1 || isset($user->roles[12])) {
        $did = $user->field_distributor_id['und'][0];
        #$query = db_select($distTable, 'd')
        #  ->fields('d', array('did', 'name'))
        #  ->condition('d.did', $did);
        $query = db_select($custTable, 'd');
        $query->addField('d', 'CustomerNumber', 'did');
        $query->addField('d', 'CustomerName', 'name');
        $query->addField('d', 'DiscountCode', 'disc');
        $query->groupBy('did');
        $query->orderBy('name');
        $query->condition('d.CustomerNumber', $did);
        $results = $query->execute()->fetchAll();
      }
      // Provide default results of false
      else {
        $results = false;
      }
  }
  // The the user is requesting a group report with one option
  // redirect instead of displaying uneccesary select form
  if(!is_null($id) && count($results) == 1) {
    $gid = $results[0]->gid;
    drupal_goto('/distributor/regional/elite-report-v11/' . $gid);
  }
  // If we have found allowed options, send the results to be formatted 
  elseif(isset($results) && $results !== false && count($results) > 0) {
    // If there is more than one distributor to select, add the select option
    if(count($results) > 1 && $type != 'group') {
      $options[''] = 'Select a Distributor';
    }
    $options += _format_results_options($results, $type);
  }
  // Or no results options
  else {
    $options[] = 'No Options';
  } 
  
  return $options;
}

/**
 * Format the given results as a set of select options
 *
 * @param $rs array
 *  rows to be formatted
 *
 * @return array
 *  select options suitable for a form
 */
function _format_results_options($rs, $type) {
  switch($type) {
    case 'group':
      foreach($rs as $r) {
        $did = $r->did;
        $gid = $r->gid;
        $name = $r->title;
        $options[$gid] = "{$name} ({$did})";
      }
    break;
    default:
      
      foreach($rs as $r) {
        $did = $r->did;
        $name = $r->name;
        
        $elite = (isset($r->disc) && $r->disc == 'DISC05') ? ' - ELITE DISTRIBUTOR' : '';
        $options[$did] = "{$name} ({$did}){$elite}";
      }
  }
  
  return $options;
}
