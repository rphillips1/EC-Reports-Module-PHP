<?php
/**
 * @file
 * The ecreports module sales view, which cotrols the sales views of 
 * the ecreports module.
 *
 * the sales view is a pluggable view using the MVC paradigm for the 
 * ecreports module.
 */

/** 
 * Provide the initial display of the slaes view plugin
 * for the ecreports module.
 *
 * @return object
 */
function ecreports_view_sales() {
  // Initialization items
  module_load_include('inc', 'ecreports', 'ecreports.controller');
  $conditions = array();
  
  // Determine and store any url query parameters
  if(isset($_GET['did'])) {
    $_GETtype = check_plain($_GET['did']);
    // Add the provided form entry to $conditions      
    $conditions[] = array(
      'col' => 'did',
      'val' => check_plain($_GETtype),
      'op' => '=',
      'fetch' => 'All'
    );
  }
  
  if(isset($_GET['year'])) {
    $_GETtype = check_plain($_GET['year']);
    // Add the provided form entry to $conditions      
    $conditions[] = array(
      'col' => 'year',
      'val' => check_plain($_GETtype),
      'op' => '=',
      'fetch' => 'All'
    );
  }
  
  if(isset($_GET['month'])) {
    $_GETtype = check_plain($_GET['month']);
    // Add the provided form entry to $conditions      
    $conditions[] = array(
      'col' => 'month',
      'val' => check_plain($_GETtype),
      'op' => '=',
      'fetch' => 'All'
    );  
  }
  
  // Provide a simple view data interface via the URL variable view
  // Per a request from John, this will display the current categories 
  // for our tool classes.
  if(isset($_GET['view'])) {
      $_GETtype = check_plain($_GET['view']);
      
      switch($_GETtype) {
        case 'ClassCategories' :
          $cats = ecreports_get_class_categories('class');
          $markup = '';
          $items = array();
          
          foreach($cats as $d) {
             if(!isset($items[$d->class_type])) {
              // Initialize the lavbel, header, and rows indices
              $items[$d->class_type]['label'] = $d->category_label;  
              $items[$d->class_type]['header'] = array('Class Number', 'Class Name');
              $items[$d->class_type]['rows'] = array();
             }
             // Build the row array
             $row = array($d->class_number, $d->class_label);
             // Add the row to the rows array
             $items[$d->class_type]['rows'][] = $row;
             
          }
          
          foreach($items as $t) {
            $markup .= "<h3><em>{$t['label']}</em> Classes</h3>";
            
            $markup .= theme('table', array('header' => $t['header'], 'rows' => $t['rows']));
            
          }
          // @todo move this to the titile callback function
          drupal_set_title('Emuge Reporting: Item Class Categories');

        break;
        default;
      }
  }
  
  
  // If there are conditions, perform a search
  if(count($conditions)) {
    $results = ecreports_query($conditions, 'classes');
    // @todo find a way to not have two exit paths. Can return value be
    // assigned to $markup?
    return ecreports_view_sales_search_results_page($results); 
  }
  // If the URL contained correct varibles, $_GETtype would be set
  // If not, perform the default action and provide the search form 
  elseif(!isset($_GETtype)) {
    $form = drupal_get_form('ecreports_view_sales_form');
    $markup = drupal_render($form); 
  }
  
  return $page['#markup'] = $markup; 
}

/**
 * Function to handle request to the distributor view dashboard and provide
 * options, where applicable.
 *
 * @param string $did
 *  The distributor ID number for lookup
 *
 * @return array $page
 *  Return a $page array ready for drupal render
 */
function ecreports_view_dashboard($did) {
  module_load_include('inc', 'ecreports', 'ecreports.controller');
  $did = check_plain($did);
  $page = array();
  // Check that the value is a number and prepare the conditions variable
  if(is_numeric($did)) {
    $conditions = array(
      'did' => $did,
    );
    // invoke the query action and pass conditions and type
    $results = ecreports_query($conditions, 'distributor_new');
    
  }
  
  return $page['#markup'] = ecreports_view_dashboard_page($results);
 
}

function ecreports_view_locations($data) {
  
  $did = $data[0]->did;
  $title = $data[0]->name;
  $page = array();
  // $markup = "<a href=\"/ecreports/distributor?did={$did}\">Back</a><h3>Emuge Location Information for {$title}</h3>";
  $backLink = "<a href=\"/ecreports/distributor?did={$did}\">Back</a><h3>Emuge Location Information for {$title}</h3>";
  $locations = ecreports_create_distributor_display($data, 'locations');
  
  foreach($locations as $branch => $location) {
    // make the operand .= to add multiple locations
    $markup = '<div style="float:left; margin-left: 25px;">';
    if($branch == 0) {
      $markup .= '<em>Corporate Headquarters</em><br />';
    }
    else {
      // $markup .= '<em>Branch Location #' . $branch . '</em><br />';
      $markup .= '<em>Corporate Headquarters</em><br />';
    }
    $markup .= $location . '</div>';
    
  }
  
  return $page['#markup'] = $backLink . $markup;
}

function ecreports_view_contracts($data) {
  // This code is overridden below on line 73
  $did = $data[0]->did;
  $title = $data[0]->name;
  $page = array();
  $markup = "<a href=\"/ecreports/distributor?did={$did}\">Back</a><h3>Emuge Contract Information for {$title}</h3>";
  $header = array('End User', 'Account #', 'Start Date', 'End Date', 'Description');
  $rows = array(
    0 => array('Pratt & Whitney', '27654', '9/1/15', '9/1/16', '40% Discount on Standard Taps'),
    1 => array('Smith & Wesson', '43028', '1/1/15', '1/1/16', '35% Discount on Holders 2015 Price Book'),
  );
  
  $markup .= theme('table', array('header' => $header, 'rows' => $rows));
  
  
  // override the above code for pre release
  $markup = "<a href=\"/ecreports/distributor?did={$did}\">Back</a><br /><h2>This section is coming soon.</h2>";
  
  return $page['#markup'] = $markup;

}

/**
 * Function to match a distributor number with its entity's discount code
 *
 * @param $data array
 *  variable to provide backup data for the searches
 */

function ecreports_view_newdiscounts($data = null) {
  
  
  if(isset($_GET['rid']) && is_numeric($_GET['rid'])) {
    $id = (int)check_plain($_GET['rid']);
    $column = 'd.entity_id';
  } 
  elseif(isset($_GET['did']) && is_numeric($_GET['did'])) {
    $id = (int)check_plain($_GET['did']);
    $column = 'd.field_distributor_number_value';
  } 
    
  if(isset($id) && isset($column)) {
    $query = db_select('field_data_field_distributor_number', 'd')->fields('d', array('entity_id', 'field_distributor_number_value'));
    $query->join('field_data_field_distributor_discount_code', 'c', 'c.entity_id = d.entity_id');
    $query->join('field_data_field_distributor_name', 'n', 'd.entity_id = n.entity_id');
    $query->fields('c', array('field_distributor_discount_code_value'));
    $query->addField('d', 'field_distributor_number_value', 'did');
    $query->addField('n', 'field_distributor_name_value', 'name');
    $query->condition('d.entity_type', 'emuge_corporation_distributor');
    
    if(isset($column)) {
      $query->condition($column, $id);
    }
    try {
      $results = $query->execute()->fetchAssoc();
    } 
    catch(PDOException $e) {
      $results = false;
    }
  }
  else {
    $results = false;
  }
  
  if($results) {
    $data = array();
    $dist = entity_load_single('emuge_corporation_distributor', $results['entity_id']);
    $items = field_get_items('emuge_corporation_distributor', $dist, 'field_distributor_discount_code');
    $data['standard_discount'] = 'disc' . $dist->field_distributor_discount_code['und'][0]['value'];
    $data['name'] = $results['name'];
    $data['did'] = $results['did'];
    $markup = ecreports_view_discounts($data);
  }
  elseif(!isset($data[0]->standard_discount)) {
    $data = false;  
  }
  
  $markup = ($data) ? ecreports_view_discounts($data) : "There is no discount information";
  
  return $markup;
}

function ecreports_view_discounts($data) {
  
  $did = $data['did'];
  $title = $data['name'];
  $discount = strtolower($data['standard_discount']);
  $path = $_GET['q'];
  $rows = array();
  $table = 'emugecom_pricing_discounts_v2';
  $header = array('Class', 'Description', 'Percent');
  preg_match('/(\d+)/', $discount, $code);
  $code = is_array($code) ? $code[0] : $code;
  $links = 
      '<div class="ec-tpr-links hide-links">
        <a href="/printpdf/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link"  onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">pdf</a>
        <a href="/print/' . $path . '?did=' . $did .'" style="float:right;"  class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">print</a>
      </div>';
  $markup = "<a href=\"/ecreports/distributor?did={$did}\">Back</a>" . $links;
  $markup = $links;
  drupal_set_title("Discount Information for {$title}");
  
  $query = db_select($table, 'dsc')->fields('dsc', array('number', 'label', $discount));
  $query->condition('label', '', '!=');
  $query->condition($discount, '0.00', '!=');
  
  try {
    $results = $query->execute()->fetchAll();
  } 
  catch(PDOException $e) {
    
    $results = false;
  }
  
  if($results) {
    foreach($results as $disc) {
      if($disc->number != 99) {
        $rows[] = array($disc->number, $disc->label, number_format($disc->$discount, 0) . '%');
      }
    }
    $markup .= "<h6>{$title} - <em>Discount Code {$code}</em></h6>";
    $markup .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('ecreportsDiscounts'))));
  }
  else {
    $discount = strtoupper($discount);
    $markup .= "<h6>No Results</h6><br />Discount Code: $discount";
  }
  
      

  return $markup;
  
}

## DEPRECATED #########################################
function _ecreports_view_discounts($data) {
  $did = $data[0]->did;
  $title = $data[0]->name;
  $ed = $data[0]->elite_discount;
  $sd = $data[0]->standard_discount;
  $md = $data[0]->multitap_discount;
  
  preg_match('/(\d+)/', $sd, $code);
  $code = is_array($code) ? $code[0] : $code;
  $path = $_GET['q'];
  $discounts = array();
  $cols = array('class_number', 'class_label', $ed, $sd, $md);
  $header = array('Class', 'Description', 'Percent');
  $links = 
      '<div class="ec-tpr-links hide-links">
        <a href="/printpdf/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link"  onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">pdf</a>
        <a href="/print/' . $path . '?did=' . $did .'" style="float:right;"  class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">print</a>
      </div>';
  
  $markup = "<a href=\"/ecreports/distributor?did={$did}\">Back</a>" . $links;
  
  drupal_set_title("Discount Information for {$title}");
  $query = db_select('emugecom_pricing_discounts', 'pd')->fields('pd', $cols);
  
  try {
    $results = $query->execute()->fetchAll();
  } 
  catch(PDOException $e) {
    $results = false;
  }
    
  if($results) {
    foreach($results as $disc) {
      if($disc->$ed = 0) {
        $discounts['Elite'][] = array($disc->class_number, $disc->class_label, $disc->$ed . '%'); 
      }
      if($disc->$sd != 0) {
        $discounts['Standard'][] = array($disc->class_number, $disc->class_label, $disc->$sd . '%'); 
      }
       if($disc->$md = 0) {
        $discounts['Multi-Tap'][] = array($disc->class_number, $disc->class_label, $disc->$md . '%'); 
      }
    }
    
    foreach($discounts as $type => $rows) {
      $markup .= "<h6>{$title} - <em>Discount Code {$code}</em></h6>";
      $markup .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('ecreportsDiscounts'))));
      
    }
  }
  else {
    $markup = 'Something went wrong, please try again.';
  }
  
  return $page['#markup'] = $markup;

}

/**
 * Display the appropriate title for the this view
 *
 * Callback for ecreports_sales_title
 *
 * @ingroups callbacks
 */
function ecreports_sales_title_callback() {
  if(isset($_GET['did'])) {
    // Get the sanitized value oof the did url varaible
    $did = check_plain($_GET['did']);
    // Use the did value to build and set the title
    drupal_set_title("Emuge Distributor #{$did} Sales Totals by Item Class");
  }
}

/** 
 * Provide the form elements for searching the ecreports
 * databases.
 *
 * @return object
 */
function ecreports_view_sales_form($form, &$form_state) {
  // Get the options for the select fields
  $options = get_ecreports_options();
  
  $form['did'] = array( 
    '#type' => 'select',
    '#title' => 'Distributor',
    '#options' => get_ecreports_options('distributors'),
    '#required' => true,
    '#prefix' => '<div class="hide-links">',
  );
  
  // This field is hidden until the additional search parameter
  // is implemented
  $form['month'] = array(
    '#type' => 'hidden',
    '#title' => 'Month',
    // comment #options as it is an invalid property of a hidden field
    // '#options' => $options['month'],
  ); 
  
  // This field is hidden until the additional search parameter
  // is implemented
  $form['year'] = array(
    '#type' => 'hidden',
    '#title' => 'Year',
    // comment #options as it is an invalid property of a hidden field
    // '#options' => $options['year'],
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('ecreports_view_sales_submit'),
    '#suffix' => '</div>'
  );
  
  return $form;
}

/**
 * Process the ecreports_view_sales form
 * 
 * Submit handler for the ecreports_view_sales form. 
 *
 * @param object $form
 *   The form object provided by Drupal for processing
 *
 * @param object $form_state
 *   The form_state object passed by reference for processing and updating
 *
 */
function ecreports_view_sales_submit($form, &$form_state) {
  // Sanitize url input
  $did = check_plain($form_state['values']['did']);
  $q = check_plain($_GET['q']);
  // Set the page title and redirect
  if(preg_match('/sales/', $q) == true && $did != null) {
    drupal_set_title('Distributor #' . $did . ' Sales Totals by Item Class');
    drupal_goto('/ecreports/sales', array('query' => array('did' => $did)));
  }
  elseif($did != null) {
    drupal_set_title('Distributor #' . $did . ' Information Page');
    
    drupal_goto('/ecreports/distributor', array('query' => array('did' => $did)));
  }
  
}

/**
 * Function to create the sales reporting html for drupal rendering
 *
 * @param array $data
 *
 * @param string $type
 */
function ecreports_view_dashboard_page($data, $type = 'default') {
  global $user;
  $markup = '';
  $gid = isset($_GET['gid']) ? true : false;
  
  switch($type) {
   
    default :
      module_load_include('inc', 'ecreports', 'ecreports.salesview');
      $markup .= ecreports_create_distributor_display($data);
      if(!isset($user->roles[12]) || $gid == true) {
        $form = drupal_get_form('ecreports_view_salesview_add_select_form');
        $markup .= '<div style="width:100%; border-top:1px solid lightgray; margin: 10px 0px; padding: 10px 0px;">' . drupal_render($form) . '</div>';
      }
  }
  
  return $markup;
}

/**
 * Process the $results data
 *
 * Use the provided search results to build a display page of tables 
 * per the provided $view type
 *
 * @param object $results
 *  The db results object for processing and converting to tables
 *
 * @param string $view
 *  The view type for displaying the page with a particular layout
 *
 * @return string
 *  A display ready html string of the page markup for rendering
 * 
 */
function ecreports_view_sales_search_results_page($results, $view = 'default') {
  // The switch allows for pluggable page layout and build parameters
  switch($view) {
    case 'default' : 
    
      // Get the redered html table markup
      $markup = _ecreports_view_sales_build_table($results, 'distributor');
    
    break;
    default;
      $markup = 'There does not seem to be any results. Try again or contact your administrator for assitnce.'; 
  }
  
  return $page['#markup'] = $markup;
}

/**
 * Convert results data to a themed table
 *
 * The provided data is organized, aggregated, and rendered into a theme table
 * 
 * @param object $data
 *  The results data to be rendered as a theme table
 *
 * @param string $type
 *  The view type for applying specific rendering and style types
 *
 * @return string
 *  A display ready html string of the page markup for rendering
 *
 *
 * @todo explain the need for and implementation of the list of
 *  variables declared at the beginning of this function.
 *
 * @todo explain the 'foreach($data['2014'] as $category => $months)' mention
 *  and its usage. It should be considered an initialization loop so should be 
 *  cached instead. 
 *
 * @todo outline and explain the current single caveat processed during
 *  this function call.
 *
 * @todo cache where applicable
 */
function _ecreports_view_sales_build_table($data, $view) {
  // define our custom view table design
  switch($view) {
    case 'distributor' :
      // initialize the needed variables for this function @todo explain

      $cc = ecreports_get_class_categories('categories');
      $dist = $data['2014'][0][0]->distributor;
      $did = $data['2014'][0][0]->did;   
      // in case we would like to come back
      # $header = array('','January','February','March','April','May','June','July','August','September','October','November','December');
      $header = array('', 'YTD Oct','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
      $cells = array();
      $temp = array();
      $markup = '';
      $title = '';
      $attributes = array('class' => array('ecreports'));
      // Initialized cont.
      $multitapRows = null;
      // Initialized cont.
      $yearlyTotalRows = array(
        0 => array(2014, 0),
        1 => array(2015, 0),
        2 => array('Difference', 0),
        3 => array('Change%', 0),
      );
      
      // Declare the report title
      $title = "<h6>{$dist} #{$did} Sales Report by Item Category</h6> ";
    
      foreach($data['2014'] as $category => $months) {
        // Initialize variables for this loop @todo explain
        $yearTotal14 = 0;
        $yearTotal15 = 0;
        $cat = $cc[$category]->category_label;
        $rows = array(
          0 => array(2014, 0),
          1 => array(2015, 0),
          2 => array('Difference', 0),
          3 => array('Change%', 0),
        ); 
    
        foreach($months as $m => $vals) {
          $T14 = (float)$data[2014][$category][$m]->total;
          $T15 = (float)$data[2015][$category][$m]->total;
          $change = ($T14 != 0) ? ($T15-$T14)/$T14 * 100 : '-'; 
          $addChange = ($change != '-') ? $change : 0;
          
          $yearlyTotalRows[0][$m+2] += $T14;
          $yearlyTotalRows[1][$m+2] += $T15;
          $yearlyTotalRows[2][$m+2] += ($T15 - $T14);
          $yearlyTotalRows[3][$m+2] = $addChange;
          
          // Apply a caveat that asks we also aggregate
          // the MultiTap class while leaving it in data
          if(isset($data[2014][$category][$m]->CLASS14)) {
            if(is_null($multitapRows)) {
              // Initialized cont.
              $multitapRows = array(
                0 => array(2014, 0),
                1 => array(2015, 0),
                2 => array('Difference', 0),
                3 => array('Change%', 0),
              );
            }
            
            $MT14 = (float)$data[2014][$category][$m]->CLASS14;
            $MT15 = (float)$data[2015][$category][$m]->CLASS14;
            $change = ($MT14 != 0) ? ($MT15-$MT14)/$MT14 * 100 : '-'; 
            $addChange = ($change != '-') ? $change : 0;
            //$change = ($MT15-$MT14)/$MT14 * 100;
            $multitapRows[0][$m+2] += $MT14;
            $multitapRows[1][$m+2] += $MT15;
            $multitapRows[2][$m+2] += ($MT15 - $MT14);
            $multitapRows[3][$m+2] = $addChange; 
          }
          
          $change = ($T14 != 0) ? ($T15-$T14)/$T14 * 100 : '-';
          // Append the latest value to the row array
          $rows[0][] = $T14;
          $rows[1][] = $T15;
          $rows[2][] = $T15 - $T14;
          $rows[3][] = $change;
        } // end of foreach month
        // add the multittap string to the category
        if(!is_null($multitapRows)) {
          $cat .= ' (Including Multi-Tap)';
        }
        
        $markup .= "<h6 class=\"ecreports\">$cat Sales</h6>";
        // add formatting and tally the tyd totals for this table
        ecreports_format_sales_rows($rows, "$cat Sales");
        $markup .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => $attributes));
        // Multitap caveat function check and logic
        if(!is_null($multitapRows)) {
          // add formatting and tally the tyd totals for this table
          ecreports_format_sales_rows($multitapRows, 'Multitap Sales');
          $markup .= "<h6 class=\"ecreports\">Multi-Tap (Only) Sales</h6>";
          $markup .= theme('table', array('header' => $header, 'rows' => $multitapRows, 'attributes' => $attributes), array('width' => '100%'));
          $multitapRows = null;
        }
      }
      
      $path = $_GET['q'];
      $did = $_GET['did'];
    $links = 
      '<div class="ec-tpr-links hide-links">
        <a href="/printpdf/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">pdf</a>
        <a href="/printmail/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">email</a>
        <a href="/print/' . $path . '?did=' . $did .'" style="float:right;" class="boxed-button ec-tpr-link" onclick="window.open(this.href, \'\', \'resizable=yes,status=yes,location=no,toolbar=no,menubar=yes,fullscreen=no,scrollbars=yes,dependent=no,width=900,left=25,height=750,top=25\'); return false;">print</a>
      </div>';
     
        
      $form = drupal_get_form('ecreports_view_sales_form');

      //add the yearly totals to the begining of the page
      ecreports_format_sales_rows($yearlyTotalRows, 'Yearly Sales');
      $yearlyMarkup = theme('table', array('header' => $header, 'rows' => $yearlyTotalRows, 'attributes' => $attributes));
      $yearlyMarkup = "<h6 class=\"ecreports\">Yearly Sales Totals for $dist</h6>" . $yearlyMarkup;
      $title = "{$dist} #{$did} Sales Reporting Portal";
      $backLink = "<a href=\"/ecreports/distributor?did={$did}\">Back</a>";
      drupal_set_title($title);
      $markup = $backLink. $links . $yearlyMarkup . $markup;
      
       

    break; // case 'distributor'
    
    default;
  } // switch($view)
   
  return $markup;
  
}

/**
 * Format table rows for display
 *
 * Add currency labels, number formatting, and specific styles to a table row
 *
 * @param array $rows
 *  A collection of rows, passed by reference, to be formatted for display
 *
 * ############################################################################
 * POTENTIALLY DEPRECATED PER REWRITE OF THIS ALOGORITHM IN THE CONTROLLER FILE
 * ############################################################################
 */
 
function _ecreports_format_sales_rows(&$rows, $title = null) {
   $monthKey = 4;
   #
   #
   foreach($rows as $key => $r) {
    foreach($r as $rkey => $c) {
      // As we loop, we only want to format the monthly dollar amounts
      // which begin at index 2 
      if($key <= 2 && $rkey >= 2) { 
          $MT14 = (float)$rows[0][$rkey];
          $MT15 = (float)$rows[1][$rkey];
          // if the monthKey is less than the rkey add the totals or add -
          if($rkey <= $monthKey) {
            $rows[0][1] += $MT14;
            $rows[1][1] += $MT15;
            $rows[2][1] += ($MT15 - $MT14);
          }
        if(!isset($rows[3][$rkey])) {
          $change = ($MT15 - $MT14)/$MT14 * 100;
          $rows[3][$rkey] = $change;
        } // endif !isset($rows)
        $rows[$key][$rkey][1] += $rows[$key][$rkey];
        $change = isset($change) ? $change : $rows[3][$rkey];
        $rows[3][$rkey] = ($rkey <= $monthKey || $key == 0) ? number_format($change, 1) : '-';
        $rows[$key][$rkey] = ($rkey <= $monthKey || $key == 0) ? number_format($rows[$key][$rkey], 0) : '-';
        unset($change);
      } 
      /*
      elseif($key == 2 && $rkey <= 2) {
        $rows[$key][$rkey] = number_format($rows[$key][$rkey], 0);
        
      } 
      elseif($key == 3 && $rkey <= 2) {
        $TM14 = (float)$rows[$key][$rkey];
        $change = '';
        $rows[$key][$rkey] = number_format($TM14, 1) . '%';
      }
      */
    } // end of foreach $r
  } // end of foreach $rows
  $rows[3][1] = number_format(($rows[1][1] - $rows[0][1]) / $rows[0][1] * 100, 1);
  $rows[0][1] = number_format($rows[0][1], 0);
  $rows[1][1] = number_format($rows[1][1], 0);
  $rows[2][1] = number_format($rows[2][1], 0);
}

/**
 * Alter the block used for the ECR Sales sidebar menu
 * 
 * Replace the placeholder value %did with the appropriate
 * value from the url. The overridden block DELTA is 1 here in
 * development. This alter also adds the Elite Report link to the appropriate
 * distributors with disc05 as a discount code. 
 * NOTE This exists as block DELTA 51 on the live site. The
 * function will need to be renamed if cloned, as is, to the live site.
 *
 * @todo
 *  define this block in code in its entirety. 
 *
 * @param obj $data
 *  The block data object reference to be altered
 *
 * @param obj $block
 *  the block object containing the block DELTA data
 *
 */
function ecreports_block_view_block_51_alter(&$data, $block) {
  // Load the module file to call the _is_elite function
  module_load_include('module', 'ecreports', 'ecreports');
  // Check and set the $did variable is the url contains good data.
  $did = (isset($_GET['did'])) ? check_plain($_GET['did']) : null;
  $rid = (isset($_GET['rid'])) ? check_plain($_GET['rid']) : null;
  $emuge = array(43360, 8151, 31901);
  // Check to see if the did is an elite customer
  $elite = _ecreports_is_elite($did);
  // Build the url context
  $q = (is_null($did)) ? '' : "?did={$did}";
  
  if(!is_null($rid)) {
    $q .= "&rid={$rid}"; 
  }
  // Replace the placeholders with the contextual link or an empty string
  $data['content'] = preg_replace('/%did/', $q, $data['content']);
  // If the link calls for it, simply replace the num value with the did without query
  $data['content'] = preg_replace('/%num/', $did, $data['content']);
  // Add the elite link to elite distributors and link to the report
  if($elite) {
    $link = in_array($did, $emuge) ? '/ecreports?gid=true&did=' . $did : '/distributor/elite-report-v11/' . $did;
    $eliteLink = '<li><a class="boxed-button emuge-product" href="' . $link . '">Elite Report</a></li></ul>';
    $data['content'] = preg_replace('/<\/ul>/', $eliteLink, $data['content']);
  }
}

/**
 * Alter the block used for the Distriubotor Creation landing page
 * 
 * Replace the placeholder value %did with the appropriate
 * value from the url. The overridden block DELTA is 1 here in
 * development. This alter also adds the Elite Report link to the appropriate
 * distributors with disc05 as a discount code. 
 * NOTE This exists as block DELTA 51 on the live site. The
 * function will need to be renamed if cloned, as is, to the live site.
 *
 * @todo
 *  define this block in code in its entirety. 
 *
 * @param obj $data
 *  The block data object reference to be altered
 *
 * @param obj $block
 *  the block object containing the block DELTA data
 *
 */
function ecreports_block_view_block_54_alter(&$data, $block) {
  
  $q = preg_split('/\//', check_plain($_GET['q']));
  $q = (integer)$q[count($q) - 1];
  #
  $q = ($q != 0) ? $q : 201400;
  #$d = entity_load_single('emuge_corporation_distributor', $q);
  #$wrapper = $d->wrapper();
  #$d = entity_metadata_wrapper('emuge_corporation_distributor', $q);
  #$d = $d->entityInfo();
  #$name = $d['label'];
  #
  $data = preg_replace('/%num/', $q, $data);
  #$data = preg_replace('/%name/', $name, $data);
}