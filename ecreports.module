<?php

/**
 * @file 
 * The ecreports MVC model file for interacting with the data tables
 * and providing results to the MVC controller 
 */

// Inlclude the MVC conroller and known views for the model
module_load_include('inc', 'ecreports', 'ecreports.controller');
module_load_include('inc', 'ecreports', 'ecreports.sales');

/**
 * Implements hook_menu().
 *
 * Drupal requires that the hook_menu function be defined in the hook.module file
 * of the project. Consequently, this hook_menu() definition relies on a type to 
 * be defined in the url at the array[1] position for invokation.
 *
 * @return array
 *  Return the $menu definitions per hook_menu()
 *
 * @see ecreports.controller.inc
 * @see hook_menu()
 */
function ecreports_menu() {
 
 $items['ecreports'] = array(
  'title' => 'Distributor Reporting',
  'title callback' => 'ecreports_title_callback',
  'page callback' => '_ecreports_url_invoke',
  'access callback' => '_ecreports_check_access',
  #'access callback' => false,
  'file' => 'ecreports.controller.inc',
 );
 
 $items['ecreports/update/db/%'] = array(
  'title' => 'Emuge Reporting Application Data Update',
  'page callback' => '__update_test_data',
  'page arguments' => array(3),
  'access callback' => '_ecreports_check_access',
 );
// Return the $items array defining the menu entry
 return $items; 
}


/**
 * @defgroup ecreports_model_data_providers Functions that provide model data 
 * 
 * @{ 
 * These functions provide database search, results, and default options 
 * to the reqeusting controller. 
 */

/**
 * Prepare the provided query for execution
 *
 * Using the conditions provided by the MVC controller, the 
 * model organizes the query before provoding it to the
 * _ecreports_execute_query function. This provides a final method for 
 * applying caveats as defined by the $view configuration options.
 *
 * @param array $conditions
 *  Array of conditions for preparation per the $view variable
 *
 * @param string $view
 *  Variable to invoke the query switch configuration defined in this view
 *
 * @return object
 *  Returns a set of query objects suitable for having the ->execute()
 *  and ->fetchTYPE() methods invoked
 *
 * @todo check on caching the classes db fetch as it does not 
 *  change often.       
 * @todo better document the method with which this demonstrates correct
 *  MVC model implementation.
 */
function _ecreports_prepare_query($conditions, $view) {
  // This switch allows for the application of specific code 
  // that is related to the view that is utilizing it. The addition
  // of the specific code is what makes this pluggable, as a switch
  // provides a discreet container to shape the views query.
  switch ($view) {
    case 'classes' :
      // classes query configuration groups the results by
      // class category, year, and month.
      $catQuery = db_select('emugecom_pricing_discounts', 'pd')
        ->fields('pd', array('class_number','class_label'));
      $catQuery->join('emugecom_item_class_categories', 'cc', 'pd.class_type = cc.id');
      $catQuery->addField('cc', 'category_label', 'category');
      $catQuery->addField('cc', 'id', 'category_id');
      
      // No need for a try catch as there is no dynamic data
      $classes = $catQuery->execute()->fetchAll();
      
      if($classes) {
        foreach(array(2014, 2015) as $y) {
          // Initialize the $prepCats varialbe during each interation
          $prepCats = array();
          
          foreach($classes as $class) {
            // prepare the variables
            $cid = $class->category_id;
            $cat = $class->category;
            $classLabel = $class->class_label;
            $classNumber = $class->class_number;
            
            // initialize the array buckets and populate the admins and 
            // checking that the varaible does not exists so as to not 
            // overwrite
            if(!isset($prepCats[$cid])) {
              $prepCats[$cid]['#name'] = $cat;
              $prepCats[$cid]['#id'] = $cid;
              $prepCats[$cid]['labels'] = array();
              $prepCats[$cid]['classes'] = array();
              $prepCats[$cid]['columns'] = array();
            }
            // populate the buckets. We can assume they exists per the 
            // previous if block
            $prepCats[$cid]['labels'][] = $classLabel;
            $prepCats[$cid]['classes'][] = $classNumber;
            $prepCats[$cid]['columns'][] = 'CLASS' . $classNumber;
          }
          
          // Using the prepCats buckets, build a queries that properly aggregates
          // the class categories per the category table definition 
          foreach($prepCats as $c) {     
            $tempQuery = db_select('emugecom_ecreports_sales', 'ics');
            $tempQuery->fields('ics', array('month', 'year', 'did'));
            $tempQuery->join('customer_data', 'cd', 'ics.did = cd.CustomerNumber');
            $tempQuery->addField('cd', 'CustomerName', 'distributor');
            // Group data by month per this view type requirement
            $tempQuery->groupBy('month');
            
            // add the SUM expression to our query object dynamically
            // based on the categorhy table definition
            foreach($c['columns'] as $col) {
              // add the SUM expression to the particular month columns
              // as this view aggregates the data by month
              $tempQuery->addExpression("SUM({$col})", $col);
              // determine if this is the beginning of the expression
              // string so as to not add a prefixed '+'
              if(!isset($exp)) {
                $exp = "$col";
              } 
              // Add the prefixed column if the variable has been initialized
              else {
                $exp .= " + $col"; 
              }
            }
            // Add the concatinated SUM $exp expression to the query object 
            // This allows for dynamic aggregation of item classes based on 
            // the class category
            $tempQuery->addExpression("SUM({$exp})", 'total');
            // Add the current iteration year to the query object
            $tempQuery->condition('year', $y);
            
            // Add additional prepared conditions as passed via the $condtions parameter
            foreach($conditions as $cd) {
              $tempQuery->condition($cd['col'], $cd['val'], $cd['op']); 
            }
            // Add the prepared $tempQuery to the return variable array
            $queries[$y][] = $tempQuery;     
            unset($exp);    
          }
        }
      }
      
    break;
    // Prepare a distributor search query
    case 'distributor' :
      #$table = 'emugecom_ecdistributor_entity';
      $table = 'emugecom_ecdistributor_properties';
      $queries = db_select($table, 'd')->fields('d')->condition('did', $conditions['did']);
      $queries->join('emugecom_state_info','s', 'd.state = s.state_id');
      $queries->fields('s');
      $tq = $queries;
      $r = $tq->execute()->fetchAll();

    break;
    // Prepare new distributor search query
    case 'distributor_new' :
      // Distributor information tables
      $name_table = 'field_data_field_distributor_name';
      $did_table = 'field_data_field_distributor_number';
      $states_table = 'emugecom_state_info';
      
      $queries = db_select($did_table, 'd');
      $queries->join($name_table, 'n', 'd.entity_id = n.entity_id');
      $queries->addField('d', 'field_distributor_number_value', 'did');
      $queries->addField('n', 'field_distributor_name_value', 'name');
      
      if(isset($conditions['did'])) {
        $queries->condition('d.field_distributor_number_value', $conditions['did'], '=');
        $queries->condition('d.entity_type', 'emuge_corporation_distributor', '=');
      }
      
    break;
    // NEW QUERY for rebuild of search and results process 11.12.15 rp
    case 'class_sales' :
      $queries = db_select('emugecom_ecreports_sales', 'cs')->fields('cs')->condition('did', $conditions['did']);
      
    break;
    case 'locations' :
      
    break;
    default :
  }
  // return queries suitable for execution
  return $queries;
}

 
/**
* function to perform a query againt the module table.
* @param mixed $conditions
*   The sanitized conditions of the query to be executed
*
* @param array $options
*   Available options for the query
*     'config' => $string  invoke a predefined set of addExpression configurations
* 
* @throws PDO/PDOException
*   Hanldes PDO exceptions that manage to bubble up to the model interface
*
* @return object
*   Returns a query results object with correct search parameters or a
*   or a PDOException object in the event of bad $query data
*/
function _ecreports_execute_query($query, $options = array()) {
  // Wrap the ->execute() call in a try catch
  try{
	// Attempt to fire the query
    $r = $query->execute()->fetchAll();
  }
  catch(PDOException $e) {
	// Retrun a triggered exception
    $r = $e->getMessage();
  }
  // Return the results of a successful query
  
  return $r;
}


/**
 * Function to provide select options of this model
 *
 * @param optional string $type
 *  String value to determine the list type to be returned
 *    Available options:
 *    null - return an object containing all options
 *    distributors - @todo implement
 *    month - @todo implement
 *    year - @todo implement
 *    classes - @todo implement
 *
 * return object $return
 *  Retur a search results object or false if failed
 */
function _ecreports_get_select_options($type = null) {
  // Table holding the item class options
  $validTypes = array(
    'distributors' => 'did', 
    'month' => 'month',
    'year' => 'year',  
  );  
  
  switch($type) {
    case null : 
      $table = 'emugecom_ecreports_sales';
      // Sanitize array for the addition of specific options 
      // requests.
      // If no type is specified, build the $options array
      // and provide all select options
    
        // Both initializes the variable and provides a default option
        $options['month']['any'] = 'Any';
        $options['year']['any'] = 'Any';
        
        foreach($validTypes as $key => $validType) {
            // Build the query that provides the distinct values
            // of the given columns for the populating a form select
            // field
            $query =  db_select($table, 't')->fields('t', array($validType))->distinct();
            $values = $query->execute()->fetchCol();
            // Add the values to the $options array
            foreach($values as $val) {
              $options[$key][$val] = $val;  
            }
         }
  
    break;
  
    case 'distributors' :
      $table = 'emugecom_ecdistributor_properties';
      $query = db_select($table, 'd')->fields('d', array('did', 'name', 'elite_discount'));
      $values = $query->orderBy('name')->execute()->fetchAll();
      
      foreach($values as $val) {
        $es = ($val->elite_discount == 'CODE5') ? ' ##ELITE##' : '';
        $strVal = $val->name . ' (' . $val->did . ')'; 
        if($val->elite_discount == 'CODE5') {
          $options[$val->did] = $strVal . $es;  
        }
      }
      
    break;
    default;
  }
  // Set $return to false if $options has not been set
  $return = isset($options) ? $options : false;
  return $return;
}

/**
 * Function to provide category information about the model
 *
 * @param string $type
 *  String value to determine the category type to be returned
 *    Available options:
 *    class - returns the coloquial collection of classes that belong
 *    loosely, to a group. This includes the catch all category Miscellaneous  
 *
 * @return ojb | bool
 *  returns the search results object, the exception value, or false if the query
 *  build fails. 
 */
function _ecreports_get_categories($type) {
  switch($type) {
    case 'class' :
      // Create a query object of the discount table
      $query = db_select('emugecom_pricing_discounts', 'pd');
      // Join the item class category table on type
      $query->join('emugecom_item_class_categories', 'cc', 'pd.class_type = cc.id');
      // Select the applicable fields for each table
      $query->fields('pd', array('class_number', 'class_label', 'class_type'));
      $query->fields('cc', array('category_label'));
    break;
    
    case 'categories' :
      $query = db_select('emugecom_item_class_categories', 'cc')->fields('cc', array('id', 'category_label'));
    break;
    default; 
  }
  
  // Return search results or false for an error. This method allows 
  // for a singular return path from this function.
  if($query) {
    // Changed to the module function query invokation rather
    // than inline as it was. One advantage is the try catch code 
    // included in my function call. I do not have to clutter my code 
    // with multiple instances of them.
    $return = _ecreports_execute_query($query);
  } 
  else {
    $return = false; 
  }
  
  return $return;
}

/**
 * @} End of "defgroup ecreports_model_data_providers". 
 */
 
function XXXis_elite($did) {
  array();
  $table = 'emugecom_ecdistributor_properties';
  
  $query = db_select($table, 'd')->fields('d');
  $query->condition('did', $did);
  $query->condition('standard_discount', 'DISC05');
  
  try {
    $results = $query->execute()->fetchAll();
  } catch(PDOException $e) {
    $results = 0;
    
  }
  
  if(count($results)) {
    return true;
  }
  else {
   return false; 
  }
}

function _ecreports_is_elite($CustomerNumber = NULL) {
    # Elite status list of distributor numbers
    $elite_accounts = array(43360, 51640, 2340, 8151, 31901, 4475, 12335, 74966, 84063);
    
    return in_array($CustomerNumber, $elite_accounts);
}
 

/**
 * WARNING - DESTRUCTIVE FUNCTION
 ********************************
 *
 * This funtion will update the existing records in the emugecom_reports_item_class_sales 
 * table with new, random dummy data. Added the OFF prefix to the function to prevent accidental 
 * invokation.
 *
 * USE WITH CAUTION
 *
 ********************************
 * WARNING - DESTRUCTIVE FUNCTION 
 */
 function OFF__update_test_data($key = null) {
  
  // code to udpate existing values
  if($key == 'IAmSure') {
    $table = 'emugecom_reporting_item_class_sales';
    $markup = '<h1>###### DATABASE UPDATING IN PROGRESS #######</h1>';
    #### DETERMINE THE PARAMETERS ####
    // get the number of records and the current
    // class names that exist in the db
    $query = db_select($table, 'ics');
    // gather all fields for column names
    $query->fields('ics');
    // determine the total records via the highest id number
    $query->addExpression('MAX(id)', 'Records');
    $maxCols= $query->execute()->fetchAssoc();
    
    // expression value to the record count
    $records = $maxCols['Records'];
    // initialize the classes array
    $classes = array();
    // gather and assing the current classes to array
    foreach($maxCols as $key => $val) {
      if(preg_match('/CLASS/', $key) != false) {
        $classes[] = $key;
      }
    };
    
    # 
    #### USE THE PARAMETERS TO UPDATE THE RECORD CLASSES ####  
    $count = 0;  
    for($id = 1; $id <= $records; $id++) {
      $fields = array();
      $amount = 100;
      
      foreach($classes as $class) {
        #$amount = ($amount == 1) ? 2 : 1;
        $fields[$class] = $amount;
      };
       
      $update = db_update($table);
      $update->condition('id', $id);
      $update->fields($fields);
      
      try {
        $r = $update->execute();
        
        $count++;
      } catch(PDOException $e) {
        $markup .= $e;
        return $page['#markup'] = $markup;
      }
      
    }
    $markup .= "$count Records Updated";
  }
  
  if($key == 'NewRecords') {
    // get all of the distributor numbers
    // get all of the class values
    // for each month 1 - 12 in 2014 and 2105
    //    add a random number for the amount in 2014
    //    change the number by a small percent
    //    add the small change to the 2015 amount
    
    # declare table variables
    $distTable = 'emugecom_ecdistributor_entity';
    $salesTable = 'emugecom_ecreports_sales';
    $classes = array();
    # get all distributor names from table 
    $query = db_select($distTable, 'd')->fields('d', array('did'));
    $dists = $query->execute()->fetchAll();
    $query = db_select($salesTable, 'c')->fields('c')->range(0, 1);
    $cols = $query->execute()->fetchAssoc();
    
    // set the col names for classes
    foreach($cols as $key => $val) {
      if(preg_match('/CLASS/', $key) != false) {
        $classes[] = $key;
      }
    };
    foreach($dists as $did => $val) {
      
    }
    
    foreach($dists as $key => $val) {
      $did = $val->did;
      #
      for($i = 1; $i <= 12; $i++) {
          $fields = array(
            'did' => $did,
            'month' => $i,
          );
        foreach(array(2014, 2015) as $y) {
          $fields['year'] = $y;
          foreach($classes as $class) {
            $amt = rand(500, 600);
            $fields[$class] = $amt;
          }
         
         $insert = db_insert($salesTable)->fields($fields);
         try {
           $r = $insert->execute();
             
         } 
         catch(PDOException $e) {
          $markup = $e;
         }
        }
      }
    }
    
    $markup = 'Insert New Records Page';
  }
   return $page['#markup'] = $markup;
}