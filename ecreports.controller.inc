<?php

/**
 * @file
 * The ecreports module MVC Controller for managing view requests and
 * invoking the model methods. Provides the model results to the requesting view 
 */

/**
 * Callback function to invoke provided views and thier workflow. This setup is
 * an attempt to utilize the MVC methods. The menu entry remains
 * deliberatley generic and provides a sudo hooking system for the views to interface.
 * The calling functions exists in the hook_menu() function (which must reside in hook.module)
 * but I beleive MVC would call for the 'invoker' should exist here. To this end, the menu
 * entry is designed to pass off all invocations to this function. See ecreports_menu().
 *
 * The following two functions exists as part of the above paradigm. They are the invokers
 * being called by hook_menu() and are designed to trigger the appropriate views hooked 
 * functions.
 * 
 * _ecreports_check_acecss()
 * _ecreports_title_callback() 
 *
 * @param string $type
 *  The machine name of the view to be invoked by a view. 
 * 
 * @return mixed 
 *    returns an appropriate object defined by the view or a sudo 404 page
 *
 * @todo explain the use of the MVC approach here. Using $type to build the 
 *  function that our plugged in view has aggreed to provide per the model 
 *  parameters.
 */ 
function _ecreports_url_invoke($type = null) {
  /**
   * override for the new salesview interface
   */
  // These valid types define the interfaces that will be included
  // in this and future versions of this app
  // Added salesview to types 12/2/15 rp
  // Added newdiscounts to types 12/30/15 rp
  $validTypes = array('contracts', 'locations', 'discounts', 'sales', 'salesview', 'newdiscounts');
  // Build the interface function name
  $function = "ecreports_view_{$type}";
  // Get any available url variables
  $did = isset($_GET['did']) ? check_plain($_GET['did']) : null;
  // Attempt to load the interface file @todo this should check for existence
  // before loading.
  module_load_include("inc", "ecreports", "ecreports.{$type}");
  // Check and return a correct iterface function call
  // SALES
  if(function_exists($function) && $type == 'sales' && is_numeric($did)) {
    return $function(); 
  } 
  // CONTRACTS, LOCATIONS, DISCOUNTS
  elseif(is_numeric($did) && in_array($type, $validTypes)) {
    // The $type variable is verified via the in_array call
    $function = "ecreports_view_{$type}";
    // This is not variable, therefore, does not need a 'check first' approach
    module_load_include('inc', 'ecreports', 'ecreports.controller');
    // Sanitize the url value
    $did = check_plain($did);
    // Check that the value is a number and prepare the conditions variable
    if(is_numeric($did)) {
      $conditions = array(
        'did' => $did,
      );
    }
    // invoke the query action and pass conditions and type
    $results = ecreports_query($conditions, 'distributor');
    // Attempt to invoke any matching interface as a fallback method
    if(function_exists($function)) {
      return $function($results);
    }
  }
  elseif(isset($did)) {
    // Return the dashboard view if a valid $did is provided
    return ecreports_view_dashboard($did);
  }
  elseif(function_exists($function)){
    return $function();
  }
  else {
    module_load_include('inc', 'ecreports', 'ecreports.salesview');
    // Provide the user the default sales distributor select form if all else fails
    $form = drupal_get_form('ecreports_view_salesview_add_select_form');
    $markup = '<h3>Welcome to the Emuge Reporting Web Application<h3><p>Please select a distributor<p>' . drupal_render($form);
      
    return $page['#markup'] = $markup; 
  }
}

/**
 * Check user access to this part of the application
 * 
 * Function to provide access checks for a user of this app
 *
 * @todo Add applicable logic to the function to provide correct access rights.
 *  The current setup simply returns true for development and testing purposes. - DONE 12/9/15 rp
 *
 * @return bool
 *  A boolean value that dictates if access is provided or not
 */ 
function _ecreports_check_access() {
  
  // Gather information about the user and the url
  global $user;
  $did = isset($_GET['did']) ? check_plain($_GET['did']) : false;
  if(!$did) {
    preg_match('/\d{1,}/', $_GET['q'], $matches);
    $did = isset($matches[0]) ? $matches[0] : false;
  }
  // First round of security checks to make sure the user has one of the two required roles
  if(isset($user->roles[6]) || isset($user->roles[7])) {
    // If the user is a TM, we need to verify further the authorized relationship
    if(isset($user->roles[7]) && is_numeric($did)) {
      // The relationships exists on this table
      $tid_didMap = 'emugecom_tid_did_map';
      // variable to hold the users TM numbers
      $tids = array();
      $TM = user_load($user->uid);
      // Loop through the loaded user and add thier tmns to the array
      foreach($TM->field_emuge_tmn['und'] as $tid) {
        $tids[] = $tid['value'];
      }
      // Prepare the db or drupal object
      $db_and = db_and();
      // We first need to check the tms in the tid column
      $db_and->condition('tid', $tids, 'IN');
      // and also check the distributor number in the did both must exist
      $db_and->condition('did', $did);
      // Grab all fields
      $query = db_select($tid_didMap, 'tdm')->fields('tdm');
      // Add the AND condition
      $query->condition($db_and);
      // Try catch the sql call
      try {
        $results = $query->execute()->fetchAll();
      } 
      catch(PDOException $e) {
        $results = 0;
      }
      // Deny access Territory ID to Distributor ID matches exist
      if(count($results) == 0) {
        return false;
      }
    }
    // Allow access 
    return true;
  }
  // Temporary override of new role for testing
  elseif(isset($user->roles[12])) {
    $user = user_load($user->uid);
    $userdid = $user->field_distributor_id['und'][0]['value'];
    if($did == $userdid) {
      return true;
    }
    
  }
  else {
    // Deny access to all else
    return false;
    
  }
}

/**
 * Function to determine if the user has the role to edit a
 * distributor entity record
 *
 * @return bool
 *  Boolean value to determine user role access
 */

function _ecreports_can_edit() {
  // Gather inoformation about the user
  global $user;
  // Determine if the user has the emuge accounting role
  $return = (isset($user->roles[11])) ? true : false;
  // Return access rights to the content
  return $return;
}

/**
 * Callback function for building the reporting page title
 *
 * Function that uses $type invocation to call the view plugins title callback
 * 
 * @param string $type
 *  It appears that $type is reassigned within the function and may not 
 *  need/want to be passed as an agrument @todo fix or deprecate
 *
 * @param string $id
 *  The id of the specifc object that is to be titles for the current page
 */
function _ecreports_title_callback($type, $id = null) {
  // Gather data from the url
  $args = preg_split('/\//', $_GET['q']);
  // Look for, sanitize, and assign data from a specific url index
  $type = check_plain($args[1]);
  $id = isset($args[2]) ? check_plain($args[2]) : null;
  
  // Attempt to build function and file names of $type per
  // the MVC model parameters. 
  $function = "ecreports_{$type}_title_callback";
  $file = "ecreports.{$type}";
  
  // Get the current path of our module
  $path = drupal_get_path('module', 'ecreports');
  
  // Check for, include, and invoke the files and funcitons
  // our MVC model has agreed to fire.
  if(file_exists($path . '/' . $file . '.inc')) {
    module_load_include('inc', 'ecreports', $file);
    $function($type, $id); 
  }
  else {
    // If there is no file or function, provide a default title
    drupal_set_title('Emuge Corporation Report Portal');
  }
}

/**
 * Get the default options for properties of the model
 *
 * Function to provide a view with defualt options for searching,
 * creating, or updating a model entry
 *
 * @param string|array $items
 *  The varaible to allow customizing of the options being reutrned. The
 *  default value of null reuturns all model options.
 *
 * @return array 
 *  An indexed array of options provided to the active view
 */
function get_ecreports_options($items = NULL) {
   if(is_null($items)) {
      $options = _ecreports_get_select_options(); 
   }
   elseif(is_array($items)) {
      foreach($items as $item) {
        $options[$item] = _ecreports_get_select_options($item);
      }
   }
   else {
      $options = _ecreports_get_select_options($items); 
   }
   
   return $options;
}

/*
 * Public interface for getting the model class categories
 *
 * Provides a public interface for invocation of the categories getter
 *
 * @param string $type
 *  The obvious default choice. this function may become less specific and
 *  provide more categories of options.
 * 
 * return array
 *  An array of options suitable for form select field #options
 */
function ecreports_get_class_categories($type = 'class') {
  $cleanType = check_plain($type);
  return _ecreports_get_categories($cleanType);
}

/**  
 * Provide a single query interface for a view
 * 
 * Function to hanldle a view query submission and invoke the model search
 *
 * @param array $conditions
 *  Array that defines the query parameters of the requested search
 *
 * @param string $view
 *  The $view variables provides access to pre defined view configurations
 *  exceptions
 */
function ecreports_query($conditions, $type = 'default') {
  // Initialize the return variable
  $results = array();
  // Get the query objects for execution
  $queries = _ecreports_prepare_query($conditions, $type);
  // testing functions and potential path for update @todo remove if not needed
  #$tempQueries = _ecreports_prepare_query($conditions, 'class_sales');
  #$tempResults = _ecreports_execute_query($tempQueries);
  switch($type) {
    case 'distributor_new' :
      // The query is a single object and need only be called once
      $results = _ecreports_execute_query($queries);
      
    break;     
    default:
      // return the results to the requesting view which will
      // be a multilevel array and needs to be processed this way
      // Iterate the queries and invoke the model query
      foreach($queries as $year => $qs) {
        foreach($qs as $q) {
          // Store the query results
          $results[$year][] = _ecreports_execute_query($q);
        }
      }
  }
  return $results;
}

function ecreports_create_distributor_display($data, $view = null) {
  // Handle no results from the distributor query
  /*
  if(is_null($data)) {
    return 'That is not a valid distributor number. Please, use the select form below.';
  }
  */
  if(!is_null($data)) {
    // address
    $name = $data[0]->name;
    $did = $data[0]->did;
    $address1 = $data[0]->address;
    $address2 = $data[0]->address2;
    $city = $data[0]->city;
    $st = $data[0]->state_abbr;
    $zip = $data[0]->zip;
    // phone
    $phone = $data[0]->phone;
    $tollfree = $data[0]->phone_tollfree;
    $fax = $data[0]->fax;
    // online
    $email = $data[0]->email;
    $website = $data[0]->website;
    $eliteDisc = 'CODE00';
    $stdDisc = $data[0]->discount;
    $stdDisc = $data[0]->discount2;
  
    switch($view) {
      case 'locations' :
        $markup = array();
        
        if($address1 == '' && $address2 != '') {
          $address = '<br />' . $address2;
        }
        elseif($address2 == '' && $address1 != '') {
          $address = '<br />' . $address1;
        }
        elseif($address2 != '' && $address1 != '') {
          $address = "<br />{$address1}<br />{$address2}";
        }
        else {
          $address = '';
        }
        
        $address .= "<br />{$city}, {$st} {$zip}";
        $item .= "{$name}{$address}";  
        
        // Build the phone section
        if($phone != '' || $tollfree != '' || $fax != '') {
          $item .= '<br /><em>Contact Info:</em><br />';
          
          if($phone != '') {
            $item .= "phone: $phone<br />";
          }
          if($tollfree != '') {
            $item .= "toll-free: $tollfree<br />";
          }
          if($fax != '') {
            $item .= "fax: $fax";
          }
        }
        // process to handle sales meeting testing markup. Generate
        // a random number from 1-3 and repeat this for locations.
        $x = rand(2, 4);
        for($i = 1; $i <= $x; $i++) {
           $markup[] = $item;
        }
      break;
    
      default :
        $markup = '';
        $markup = "<h2>{$name} <br />Account #{$did}</h2>";
        if(_ecreports_can_edit()) {
          $markup .= '<a href="/distributors/list">View All</a>';
		  if(isset($_GET['rid']) && is_numeric(check_plain($_GET['rid']))) {
        $rid = $_GET['rid'];
        $markup .= ' <a href="/admin/structure/entity-type/emuge_corporation_distributor/distributor_bundle/' . $rid . '/edit?destination=/distributors/list">Edit</a>';
		  }
        }
        /*
        // Build the address section
        if($address1 == '' && $address2 != '') {
          $address = '<br />' . $address2;
        }
        elseif($address2 == '' && $address1 != '') {
          $address = '<br />' . $address1;
        }
        elseif($address2 != '' && $address1 != '') {
          $address = "<br />{$address1}<br />{$address2}";
        }
        else {
          $address = '';
        }
        
        $address .= "<br />{$city}, {$st} {$zip}";
        $markup .= "<div>{$name}{$address}";  
        
        // Build the phone section
        if($phone != '' || $tollfree != '' || $fax != '') {
          $markup .= '<div><em>Contact Info:</em><br />';
          
          if($phone != '') {
            $markup .= "phone: $phone<br />";
          }
          if($tollfree != '') {
            $markup .= "toll-free: $tollfree<br />";
          }
          if($fax != '') {
            $markup .= "fax: $fax";
          }
          
          $markup .= '</div>';
        }
        
        // build the online section
        if($email != '' || $website != '') {
          $markup .= '<div><em>Online Info:</em><br />';
          if($email != '') {
            $markup .= "<a href=\"mailto:{$emial}\">{$email}</a><br />" ;
          }
          if($website != '') {
            $markup .= "<a href=\"http://{$website}\">{$website}</a>" ;
          }
          $markup .= '</div>';
        }
        */
      }
  }
  else {
    $markup = 'That is not a valid distributor number. Please, use the select form below.';
  }
  
  return $markup;
}

/**
 * Format table rows for display
 *
 * Add currency labels, number formatting, and specific styles to a table row
 *
 * @param array $rows
 *  A collection of rows, passed by reference, to be formatted for display
 *
 */

function ecreports_format_sales_rows(&$rows, $title = null) {
  // Set the desired month to stop aggregation  
  $monthStop = 11;
  // Loop through each row
  foreach($rows as $rowsKey => $row) {
    // Loop through each cell of each row
    foreach($row as $cellKey => $cell) {
      // Determine that we are working with the correct cells
      // The first argument determines that we are working with month data
      // The second argument determines that we are working withing desired month stop
      if($cellKey >= 2 && $cellKey <= $monthStop) {
        // Add the $cell value to the yearly aggregate total
        $rows[$rowsKey][1] += $cell; 
        // If the row type is dollar amounts, format the number to 0 precision
        // If the row type is percent, format the number to 2 precision
        $precision = ($rowsKey == 3) ? 2 : 0;
        if(is_numeric($rows[$rowsKey][$cellKey])) {
          // Format the row as a number
          $rows[$rowsKey][$cellKey] = number_format((int)$rows[$rowsKey][$cellKey], $precision);
        }
      }
      // Determine if we are at an active month cell via the $cellKey
      // Determine if we are working with the 2014 row via the $rowsKey
      elseif($cellKey > $monthStop && $rowsKey == 0) {
        // If this is outside the $monthStop but still the 2014 row
        // Format the row as a number
        $rows[$rowsKey][$cellKey] = number_format((int)$rows[$rowsKey][$cellKey]);
      }
      // If we are working with other rows
      elseif($cellKey > $monthStop) {
        $rows[$rowsKey][$cellKey] = '-';
      }
    }
    // When we are in the final row, gather the last change value and format 
    // the YTD colums cells
    if($rowsKey == 3) {

      // Prevent our algorithm from dividing by zero and set the change value if attempted
      $change = ($rows[0][1] != 0) ? 
        (preg_replace('/,/', '', $rows[2][1]) / preg_replace('/,/', '', $rows[0][1])) * 100 : '-';
      // If we have succesfully divided, number format the results
      $rows[3][1] = is_numeric($change) ? number_format($change, 2) : $change;
      for($i = 0; $i <= 2; $i++) {
        // If our cell value is numeric, number format the cell
        $rows[$i][1] = is_numeric($rows[$i][1]) ? number_format($rows[$i][1]) :$rows[$i][1];
      }
    }
  }
}